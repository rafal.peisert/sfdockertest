# SfDockerTest
A simple PhpStorm project intended to test Symfony web development with Docker.

## Setup
Clone the repo, copy `.env.dist` to `.env` and edit it as appropriate. For instructions
on setting up PhpStorm to work with Docker see [this article][1].

[1]:https://blog.jetbrains.com/phpstorm/2018/08/quickstart-with-docker-in-phpstorm/

## Running commands
The `symfony` and `composer` binaries are available in the `cli` service. Run a command
using `docker-compose run`, e.g.:
```bash
$ docker-compose run --rm cli symfony
```

## Starting your Symfony project
Create your project using the Symfony binary:
```bash
$ docker-compose run --rm cli symfony local:new --version=lts --full --no-git --debug -- myawesomeproject
```

Edit the `.env` file and change `CLI_CWD` and `PUBLIC_HTML` to point to the root directory of your project:
```dotenv
CLI_CWD=./myawesomeproject
PUBLIC_HTML=./myawesomeproject
``` 

Add the necessary rewrite rules using `.htaccess` in your project's `public` directory - either by hand, or by
installing the `apache` Symfony pack:
```bash
$ docker-compose run --rm cli composer require --dev symfony/apache-pack
```
Start the stack using `docker-compose up` and point your browser to http://localhost:8000/ (assuming 8000 is what you put in the
`APACHE_PORT` environment variable). You should see the Symfony welcome page.